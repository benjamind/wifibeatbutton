#include "Arduino.h";
#include "Caleidoscope.h";
#include "LED.h";
#include "LED_Config.h";
#include "Graphics.h";
#include "BPM.h";
#include "Noise.h";

uint8_t caleidoscope = 0;

void Caleido3() {

  uint8_t beat =  beat8(globalBPM, prevBeat);
  uint8_t sinbeat = sin8(beat);
  // a new parameter set every 15 seconds
  EVERY_N_SECONDS(15) {

    dy = random16(2000) - 1000; // random16(2000) - 1000 is pretty fast but works fine, too
    dx = random16(2000) - 1000;
    dz = random16(2000) - 1000;
    scale_x[0] = random16(20000) + 4000;
    scale_y[0] = random16(20000) + 4000;
  }

  y[0] += dy + sinbeat;
  x[0] += dx;
  z[0] += dz;

  FillNoise(0);
  for (uint8_t p = 0; p < NUM_STRIPS; p++) {
    ShowLayer(p, 0, 1);
    Caleidoscope3(p);
    Caleidoscope1(p);
  }

  fadeToBlackBy(leds, NUM_LEDS, scale8(sinbeat, 50));
}
// rotates the first 16x16 quadrant 3 times onto a 32x32 (+90 degrees rotation for each one)
void Caleidoscope1(uint8_t panel) {
  for (int x = 0; x < MATRIX_WIDTH / 2 ; x++) {
    for (int y = 0; y < MATRIX_HEIGHT / 2; y++) {
      leds[XY( panel, MATRIX_WIDTH - 1 - x, y )] = leds[XY( panel, x, y )];
      leds[XY( panel, x, MATRIX_HEIGHT - 1 - y )] = leds[XY( panel, x, y )];
      leds[XY( panel, MATRIX_WIDTH - 1 - x, MATRIX_HEIGHT - 1 - y )] = leds[XY( panel, x, y )];
    }
  }
}
// mirror the first 16x16 quadrant 3 times onto a 32x32
void Caleidoscope2(uint8_t panel) {
  for(int x = 0; x < MATRIX_WIDTH / 2 ; x++) {
    for(int y = 0; y < MATRIX_HEIGHT / 2; y++) {
      leds[XY( panel, MATRIX_WIDTH - 1 - x, y )] = leds[XY( panel, x, y )];              
      leds[XY( panel, x, MATRIX_HEIGHT - 1 - y )] = leds[XY( panel, x, y )];             
      leds[XY( panel, MATRIX_WIDTH - 1 - x, MATRIX_HEIGHT - 1 - y )] = leds[XY( panel, x, y )]; 
    }
  }
}
// copy one diagonal triangle into the other one within a 16x16

void Caleidoscope3(uint8_t panel) {
  for(int x = 0; x <= CentreX ; x++) {
    for(int y = 0; y <= x; y++) {
      leds[XY( panel, x, y )] = leds[XY( panel, y, x )]; 
    }
  }
}  


// copy one diagonal triangle into the other one within a 16x16 (90 degrees rotated compared to Caleidoscope3)

void Caleidoscope4(uint8_t panel) {
  for(int x = 0; x <= CentreX ; x++) {
    for(int y = 0; y <= CentreY-x; y++) {
      leds[XY( panel, CentreY - y, CentreX - x )] = leds[XY( panel, x, y )]; 
    }
  }
}  


// copy one diagonal triangle into the other one within a 8x8

void Caleidoscope5(uint8_t panel) {
  for(int x = 0; x < MATRIX_WIDTH/4 ; x++) {
    for(int y = 0; y <= x; y++) {
      leds[XY( panel, x, y )] = leds[XY( panel, y, x )];
    }
  }

  for(int x = MATRIX_WIDTH/4; x < MATRIX_WIDTH/2 ; x++) {
    for(int y = MATRIX_HEIGHT/4; y >= 0; y--) {
      leds[XY( panel, x, y )] = leds[XY( panel, y, x )];
    }
  }
}
#define CALEIDO_NONE   0
#define CALEIDO_ROTATE 1
#define CALEIDO_VERT_MIRROR 2
#define CALEIDO_HORZ_MIRROR 4
#define CALEIDO_SCOPE1 8
#define CALEIDO_SCOPE2 16
#define CALEIDO_SCOPE3 32
#define CALEIDO_MAX 64

void ApplyCaleidoscope() {
  switch (caleidoscope) {
    case CALEIDO_ROTATE: {
      for (uint8_t p = 0; p < NUM_STRIPS; p++) {
        Caleidoscope1(p);
      }
      break;
    }
    case CALEIDO_VERT_MIRROR: {
      for (uint8_t p = 0; p < NUM_STRIPS; p++) {
        Caleidoscope2(p);
      }
      break;
    }
    case CALEIDO_HORZ_MIRROR: {
      for (uint8_t p = 0; p < NUM_STRIPS; p++) {
        Caleidoscope4(p);
        Caleidoscope2(p);
      }
      break;
    }
    case CALEIDO_SCOPE1: {
      for (uint8_t p = 0; p < NUM_STRIPS; p++) {
        Caleidoscope4(p);
        Caleidoscope1(p);
      }
      break;
    }
    case CALEIDO_SCOPE2: {
      for (uint8_t p = 0; p < NUM_STRIPS; p++) {
        Caleidoscope5(p);
        Caleidoscope4(p);
        Caleidoscope2(p);
      }
      break;
    }
    case CALEIDO_SCOPE3: {
      for (uint8_t p = 0; p < NUM_STRIPS; p++) {
        Caleidoscope5(p);
        Caleidoscope1(p);
        Caleidoscope2(p);
      }
      break;
    }
  }
}
void ClearCaleidoscope() {
  caleidoscope = 0;
}
void ToggleCaleidoscope() {
  if (caleidoscope == 0) {
    PickCaleidoscope();
  } else {
    caleidoscope = 0;
  }
}
void NextCaleidoscope() {
  if (caleidoscope == 0) {
    caleidoscope = 1;
  } else {
    caleidoscope = caleidoscope << 1;
  }
  if (caleidoscope == CALEIDO_MAX) {
    caleidoscope = 0;
  }
  Serial.print("Caleidoscope: ");
  Serial.println(caleidoscope);
}
