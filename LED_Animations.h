#ifndef LED_ANIMATIONS_H
#define LED_ANIMATIONS_H
#include "FastLED.h"
#include "LED.h"
#include "Caleidoscope.h"
#include "BPM.h"
#include "Palettes.h"
#include "Graphics.h"
#include "Noise.h"

extern uint8_t gCurrentPatternNumber;
extern uint8_t gHue;
extern uint8_t hueOffset;
extern uint8_t gHueOffset;
extern uint32_t moveTil;

// List of patterns to cycle through.  Each is defined as a separate function below.
typedef void (*SimplePatternList[])();

void setPattern(uint8_t anim, uint16_t randomSeed, uint8_t hue, uint8_t hOffset);
void nextPattern();
void randomPattern();

// define the animations
void Animation_Blocks();
void Animation_Bubbles1();
void Animation_LavaLamp1();
void Animation_MirroredNoise();
void Animation_SineWave();
void Animation_RandomWalks();
void Animation_NoiseCaleidoscope();

// basics
void Animation_ColorFlash();
void Animation_BPMPanels();
void Animation_RandomPanels();
void Animation_JugglePanels();
void Animation_CylonPanels();
void Animation_FlatColor();
void Animation_Rainbow();
void Animation_Confetti();
void Animation_Sinelon();
void Animation_BPM();
void Animation_Juggle();
void Animation_Wibbly();

// experimental
void Animation_Ghost();
void Animation_SlowMandala();
void Animation_Dots1();
void Animation_AudioSpiral();
void Animation_Dots2();

void LED_Animations_Setup();
void LED_Animations_Handle();

#endif // LED_ANIMATIONS_H
