#include "Arduino.h"
#include "LED_Animations.h"

uint8_t hueOffset = 10;
uint8_t gHueOffset = 10;

void Animation_ColorFlash() {
  uint8_t beat =  beat8(globalBPM, prevBeat);
  uint8_t wave = squarewave8(beat, 16);
  fadeToBlackBy( leds, NUM_LEDS, 40);
  if (wave > 0) {
    for (uint8_t i = 0; i < NUM_LEDS; i++) {
      leds[i] = ColorFromPalette(currentPalette, gHue+random8(64), 255);
    }
  }
}
void Animation_BPMPanels() {
  uint8_t beat = beatsin8( globalBPM/2, 64, 255);
  //uint8_t gHueOffset = beatsin8( 2, 10, 14);
  uint8_t row = 0;
  for (uint8_t y = 0; y < MATRIX_HEIGHT; y++) {
    row = y*MATRIX_WIDTH;
    for (uint8_t x = 0; x < MATRIX_WIDTH; x++) {
      uint8_t i = row + x;
      for (uint8_t panel = 0; panel < NUM_STRIPS; panel++) {
        leds[XY(panel, x,y)] = ColorFromPalette(currentPalette, gHue+(i*2), beat-gHue+(i*gHueOffset));
      }
    }
  }
}
void Animation_RandomPanels() {  
  fadeToBlackBy( leds, NUM_LEDS, 20);
  uint8_t beat =  beat8(globalBPM, prevBeat);
  uint8_t wave = squarewave8(beat, 16);

  if (wave > 0) {
    for (uint8_t i=0; i < 64; i++) {
      int pos = random16(NUM_LEDS_PER_STRIP);
      for (uint8_t p = 0; p < NUM_STRIPS; p++) {
        leds[POS(p, pos)] += ColorFromPalette(currentPalette, gHue, 255);
      }
    }
  }
}
void Animation_JugglePanels() {
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy( leds, NUM_LEDS, 20);
  byte dothue = 0;
  for( int i = 0; i < 32; i++) {
    for (uint8_t p = 0; p < NUM_STRIPS; p++) {
      leds[POS(p, beatsin16(globalBPM/2,0,NUM_LEDS_PER_STRIP-1))] |= ColorFromPalette(currentPalette, dothue, 255);
    }
    dothue += 32;
  }
}

void Animation_CylonPanels() {
  fadeToBlackBy( leds, NUM_LEDS, 64);
  uint8_t matrixWidth = MATRIX_WIDTH;
  uint8_t matrixHeight = MATRIX_HEIGHT;
  if (caleidoscope != 0) {
    matrixWidth = MATRIX_WIDTH / 2;
    matrixHeight = MATRIX_HEIGHT / 2;
  }
  uint8_t pos = beatsin16(globalBPM/2,0,matrixHeight-1, prevBeat);
  for (uint8_t x = 0; x < matrixWidth; x++) {
    uint8_t y = pos > 0 ? pos-1 : 0;
    for (; y < matrixWidth && y < pos+1; y++) {
      for (uint8_t p = 0; p < NUM_STRIPS; p++) {
        leds[XY(p, x, y)]+= ColorFromPalette(currentPalette, gHue + random8(64), 255);
      }
    }
  }
}


void Animation_FlatColor() {
  fill_solid(leds, NUM_LEDS, gHue);
}
void Animation_Rainbow() 
{
  // FastLED's built-in rainbow generator
  //fill_rainbow( leds, NUM_LEDS, gHue, 7);
  FillFromPalette(leds, NUM_LEDS, gHue, 255, 7);
}
void Animation_Confetti() 
{
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, 10);
  for (uint8_t i=0; i < 10; i++) {
    int pos = random16(NUM_LEDS);
    //leds[pos] += CHSV( gHue + random8(64), 200, 255);
    leds[pos] += ColorFromPalette(currentPalette, gHue, 255);
  }
}

void Animation_Sinelon()
{
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16(30,0,NUM_LEDS);
  //leds[pos] += CHSV( gHue, 255, 192);
  leds[pos]+= ColorFromPalette(currentPalette, gHue + random8(64), 255);
}

void Animation_BPM()
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  //CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8( globalBPM, 64, 255);
  for( int i = 0; i < NUM_LEDS; i++) { //9948
  //  leds[i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10));
    leds[i] = ColorFromPalette(currentPalette, gHue+(i*2), beat-gHue+(i*10));
  }
}

void Animation_Juggle() {
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy( leds, NUM_LEDS, 20);
  byte dothue = 0;
  for( int i = 0; i < 32; i++) {
    //leds[beatsin16(i+7,0,NUM_LEDS)] |= CHSV(dothue, 200, 255);
    leds[beatsin16(globalBPM,0,NUM_LEDS)] |= ColorFromPalette(currentPalette, dothue, 255);
    dothue += 32;
  }
}
void Animation_Wibbly() {
  // walk through the palette slowly spreading out in a mirror from the center of the strip
  const int half = NUM_LEDS/2;
  for (int i = 0; i < half; i++) {
    uint8_t beat = beatsin8( globalBPM + i*3, 64, 255);
    leds[half+i] = ColorFromPalette(currentPalette, gHue+(i*2), beat);
    leds[half-i-1] = ColorFromPalette(currentPalette, gHue+(i*2), beat);
  }
}


