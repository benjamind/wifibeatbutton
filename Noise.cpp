#include "Arduino.h"
#include "Noise.h"
#include "LED.h"
#include "Graphics.h"
#include "Palettes.h"

uint8_t noise[NUM_LAYERS][MATRIX_WIDTH][MATRIX_HEIGHT];
byte CentreX =  (MATRIX_WIDTH / 2) - 1;
byte CentreY = (MATRIX_HEIGHT / 2) - 1;
uint32_t x[NUM_LAYERS];
uint32_t y[NUM_LAYERS];
uint32_t z[NUM_LAYERS];
uint32_t scale_x[NUM_LAYERS];
uint32_t scale_y[NUM_LAYERS];
uint8_t noisesmoothing;
uint8_t colorshift;
// used for the random based animations
int16_t dx;
int16_t dy;
int16_t dz;
int16_t dsx;
int16_t dsy;

void Noise_Setup() {
  noisesmoothing = 200;
  colorshift = 0;

  // just any free input pin
  //random16_add_entropy(analogRead(18));
  random16_set_seed(12345);

  // TODO move into noise seutp
  // fill coordinates with random values
  // set zoom levels
  for(int i = 0; i < NUM_LAYERS; i++) {
    x[i] = random16();
    y[i] = random16();
    z[i] = random16();
    scale_x[i] = 6000;
    scale_y[i] = 6000;
  }
  // for the random movement
  dx  = random8();
  dy  = random8();
  dz  = random8();
  dsx = random8();
  dsy = random8();
}
void FillNoise(byte layer) {

  for(uint8_t i = 0; i < MATRIX_WIDTH; i++) {

    uint32_t ioffset = scale_x[layer] * (i-CentreX);

    for(uint8_t j = 0; j < MATRIX_HEIGHT; j++) {

      uint32_t joffset = scale_y[layer] * (j-CentreY);

      byte data = inoise16(x[layer] + ioffset, y[layer] + joffset, z[layer]) >> 8;

      uint8_t olddata = noise[layer][i][j];
      uint8_t newdata = scale8( olddata, noisesmoothing ) + scale8( data, 256 - noisesmoothing );
      data = newdata;

      noise[layer][i][j] = data;
    }
  }
}
void ShowLayer(uint8_t index, byte layer, byte colorrepeat) { 
  for(uint8_t i = 0; i < MATRIX_WIDTH; i++) {
    for(uint8_t j = 0; j < MATRIX_HEIGHT; j++) {

      uint8_t color = noise[layer][i][j]; 

      uint8_t   bri = color;

      // assign a color depending on the actual palette
      CRGB pixel = ColorFromPalette( currentPalette, colorrepeat * (color + colorshift), bri );

      leds[XY(index, i,j)] = pixel;
    }
  }
}
