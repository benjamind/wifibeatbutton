#ifndef LED_Config_h
#define LED_Config_h

#define NUM_LEDS_PER_STRIP 256
#define MATRIX_WIDTH 16
#define MATRIX_HEIGHT 16

//#define UNIT_TYPE_SUIT
//#define UNIT_TYPE_CORSET_1
#define UNIT_TYPE_CORSET_2

#define HOST_IP IPAddress(192, 168, 4, 1)
#define GATEWAY_IP IPAddress(192, 168, 0, 1)
#define SUBNET_IP IPAddress(255, 255, 255, 0)

#define CLIENT_IP_1 IPAddress(192, 168, 4, 100)
#define CLIENT_IP_2 IPAddress(192, 168, 4, 101)
//#define CLIENT_IP_3 IPAddress(192, 168, 4, 102)
//#define CLIENT_IP_4 IPAddress(192, 168, 4, 103)

#ifdef UNIT_TYPE_CORSET_1
#define UNIT_TYPE_CORSET
#define CLIENT_IP CLIENT_IP_1
#endif

#ifdef UNIT_TYPE_CORSET_2
#define UNIT_TYPE_CORSET
#define CLIENT_IP CLIENT_IP_2
#endif

#ifdef UNIT_TYPE_CORSET
#define NUM_STRIPS 1
#endif

#ifdef UNIT_TYPE_SUIT
#define NUM_STRIPS 4
#endif

#define NUM_LEDS (NUM_STRIPS * NUM_LEDS_PER_STRIP)

#endif // LED_Config_h