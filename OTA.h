// OTA setup and management

#ifndef _OTA_H
#define _OTA_H

void OTA_Setup();
void OTA_Handle();

#endif // _OTA_H
