#include "Arduino.h"
#include "LED_Animations.h"

byte pos = 0;

void Animation_SineWave() {
  // draws a since wave across the display with the bpm
  byte beat = beatsin8(globalBPM, 0, MATRIX_HEIGHT-1);
  EVERY_N_MILLISECONDS(128) {
    pos++;
    if (pos >= MATRIX_WIDTH) {
      pos = 0;
    }
  }
  fadeToBlackBy( leds, NUM_LEDS, 15);
  CRGB col = ColorFromPalette( currentPalette, gHue, 255 );
  for (uint8_t p = 0; p < NUM_STRIPS; p++) {
    leds[XY(p, pos, beat)] = col;
  }
}

