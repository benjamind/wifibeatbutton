#ifndef _STATUS_SYNC_H
#define _STATUS_SYNC_H

void Status_Sync_Setup();
void Status_Sync_Handle();

#ifdef UNIT_TYPE_SUIT
void Status_Sync_Send_Update();
#endif

#define STATUS_BPM      1       // BPM packet - uint8_t, uint32_t - bpm, sineOffset        
#define STATUS_PALETTE  2       // Palette packet - uint8_t
#define STATUS_ANIM     3       // Anim packet - uint8_t, uint16_t - palette index, randomOffset

#endif