#ifndef NOISE_H
#define NOISE_H
#include "LED.h"

////////////////////////////////////////////////////////////////////////////////
// NOISE
////////////////////////////////////////////////////////////////////////////////
#define NUM_LAYERS 3

extern uint8_t noise[NUM_LAYERS][MATRIX_WIDTH][MATRIX_HEIGHT];
extern byte CentreX;
extern byte CentreY;
extern uint32_t x[NUM_LAYERS];
extern uint32_t y[NUM_LAYERS];
extern uint32_t z[NUM_LAYERS];
extern uint32_t scale_x[NUM_LAYERS];
extern uint32_t scale_y[NUM_LAYERS];
extern uint8_t noisesmoothing;
extern uint8_t colorshift;
// used for the random based animations
extern int16_t dx;
extern int16_t dy;
extern int16_t dz;
extern int16_t dsx;
extern int16_t dsy;

void FillNoise(byte layer);
void ShowLayer(uint8_t index, byte layer, byte colorrepeat);

void Noise_Setup();

#endif