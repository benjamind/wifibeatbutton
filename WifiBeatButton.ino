#include "OTA.h"
#include "LED.h"
#include "Keyboard.h"
#include "WiFi.h"
#include "Status_Sync.h"

/*const char* ssid = "Essex_Boy";
const char* password = "kan00dle";*/

const char* ssid = "DJ_Schlaut"; 
const char* password = "kan00dle";

void setup() {
  random16_set_seed(12345);
  delay(3000); // 3 second delay for recovery
  Serial.begin(115200);

  WiFi_Setup(ssid, password);  
  LED_Setup();
  Keyboard_Setup();
  OTA_Setup();

  Status_Sync_Setup();

  Serial.println("Startup complete.");
}

void loop()
{   
  OTA_Handle();
  WiFi_Handle(ssid, password);
  Keyboard_Handle();
  LED_Handle();
  Status_Sync_Handle();
}