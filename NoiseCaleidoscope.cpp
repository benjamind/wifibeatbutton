#include "Arduino.h"
#include "LED_Animations.h"

void Animation_NoiseCaleidoscope() {
  // fade everything a bit  
  // draw some random pixels into the top left corner of the array
 
  fadeToBlackBy( leds, NUM_LEDS, 15);
  if (isBeat(32)) {
    moveTil = millis() + bpmMillis/4;
  }
  // advance the snake positions randomly
  //EVERY_N_MILLISECONDS(4) {
  if (millis() < moveTil) {
    for (byte x = 0; x < MATRIX_WIDTH/2; x++) {
      for (byte y = 0; y < MATRIX_HEIGHT/2; y++) {
        if (random8() > 240) {
          for (byte p = 0; p < NUM_STRIPS; p++) {
            leds[XY(p, x, y)] = ColorFromPalette( currentPalette, gHue, 255 );
          }
        }
      }
    }
  }
  // duplicate the random pixels around the matrix
  for (byte p = 0; p < NUM_STRIPS; p++) {
    Caleidoscope1(p);
  }
}

