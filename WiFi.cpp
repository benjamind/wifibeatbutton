#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include "WiFi.h"
#include "LED.h"
#include "BPM.h"
#include "LED_Config.h"

#define DNS_NAME "blinkything"
#define OSCDEBUG    (0)

extern CRGB leds[NUM_LEDS];

bool disconnected = true;
unsigned long connecting = 0;
/*WiFiServer server(7890);
WiFiClient client;
MDNSResponder mdns;
bool hasMDNS = false;
#define minsize(x,y) (((x)<(y))?(x):(y))*/

void WiFi_Setup(const char* ssid, const char* password) {
#ifdef UNIT_TYPE_SUIT
    WiFi_AP(ssid, password);
#endif
#ifdef UNIT_TYPE_CORSET
    WiFi_Connect(ssid, password);
#endif

  // setup mDNS
  /*if (!mdns.begin(DNS_NAME, WiFi.localIP())) {
    Serial.println("Error setting up MDNS responder!");
  } else {
    hasMDNS = true;
    Serial.println("mDNS responder started");
    Serial.printf("My name is [%s]\r\n", DNS_NAME);
    
    mdns.addService("opc", "tcp", 7890);
  }*/
  // Start the server listening for incoming client connections
  //server.begin();
  //Serial.println("Server listening on port 7890");
}
uint8_t Find_Free_Channel() {
  Serial.println("Starting WiFi scan...");
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  // pick a channel, 1
  uint8_t weakestChannel = 1;
  int weakestChannelRSSI = -100;
  uint8_t channel = 1;
  while (channel <= 13) {
    // loop over the networks we found
    bool channelFree = true;
    for (int i = 0; i < n; ++i) {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print("[");
      Serial.print(WiFi.channel(i));
      Serial.print("] ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*");

      if (WiFi.RSSI(i) > weakestChannelRSSI) {
        weakestChannelRSSI = WiFi.RSSI(i);
        weakestChannel = i;
      }
      // if the network is on this channel, increment channel and restart
      if (WiFi.channel(i) == channel) {
        channelFree = false;
        channel++;
        break;
      }
    }
    if (channelFree) {
      // we found a free channel, return it
      return channel;
    }
  }
}
void WiFi_Scan() {
  Serial.println("Starting WiFi scan...");
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  for (int i = 0; i < n; ++i) {
    // Print SSID and RSSI for each network found
    Serial.print(i + 1);
    Serial.print(": ");
    Serial.print("[");
    Serial.print(WiFi.channel(i));
    Serial.print("] ");
    Serial.print(WiFi.SSID(i));
    Serial.print(" (");
    Serial.print(WiFi.RSSI(i));
    Serial.print(")");
    Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*");
  }
}
#ifdef UNIT_TYPE_CORSET
bool hasScan = false;
void WiFi_Connect(const char* ssid, const char* password) {
  WiFi.mode(WIFI_STA);
  if (!hasScan) {
    WiFi_Scan();
    hasScan = true;
  }

  Serial.print("Connecting to ");
  Serial.print(ssid);
  Serial.print(" : ");
  Serial.println(password);

  disconnected = true;
  connecting = millis();
  WiFi.begin(ssid, password);
  WiFi.config(CLIENT_IP, GATEWAY_IP, SUBNET_IP);
  /*while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed!");
    return;
  }
  disconnected = false;*/

  // setup mDNS
  /*if (!mdns.begin(DNS_NAME, WiFi.localIP())) {
    Serial.println("Error setting up MDNS responder!");
  } else {
    hasMDNS = true;
    Serial.println("mDNS responder started");
    Serial.printf("My name is [%s]\r\n", DNS_NAME);
    
    mdns.addService("opc", "tcp", 7890);
  }
  // Start the server listening for incoming client connections
  server.begin();
  Serial.println("Server listening on port 7890");*/
}
#endif
#ifdef UNIT_TYPE_SUIT
void WiFi_AP(const char* ssid, const char* password) {
  uint8_t channel = Find_Free_Channel();
  Serial.print("Using channel ");
  Serial.println(channel);

	Serial.print("Configuring access point ");
  Serial.print(ssid);
  Serial.print(" : ");
  Serial.println(password);
  
  WiFi.softAP(ssid, password, channel);
  WiFi.softAPConfig(HOST_IP, GATEWAY_IP, SUBNET_IP);
	IPAddress myIP = WiFi.softAPIP();
	Serial.print("AP IP address: ");
	Serial.println(myIP);
}
#endif
/*
void clientEvent()
{
  static int packetParse = 0;
  static uint8_t pktChannel, pktCommand;
  static uint16_t pktLength, pktLengthAdjusted, bytesIn;
  static uint8_t pktData[NUM_LEDS*3];
  uint16_t bytesRead;
  size_t frame_count = 0, frame_discard = 0;

  if (!client) {
    // Check if a client has connected
    client = server.available();
    if (!client) {
      return;
    }
    Serial.println("new OPC client");
  }

  if (!client.connected()) {
    Serial.println("OPC client disconnected");
    client = server.available();
    if (!client) {
      return;
    }
  }

  while (client.available()) {
    switch (packetParse) {
      case 0: // Get pktChannel
        pktChannel = client.read();
        packetParse++;
#if OSCDEBUG
        Serial.printf("pktChannel %u\r\n", pktChannel);
#endif
        break;
      case 1: // Get pktCommand
        pktCommand = client.read();
        packetParse++;
#if OSCDEBUG
        Serial.printf("pktCommand %u\r\n", pktCommand);
#endif
        break;
      case 2: // Get pktLength (high byte)
        pktLength = client.read() << 8;
        packetParse++;
#if OSCDEBUG
        Serial.printf("pktLength high byte %u\r\n", pktLength);
#endif
        break;
      case 3: // Get pktLength (low byte)
        pktLength = pktLength | client.read();
        packetParse++;
        bytesIn = 0;
#if OSCDEBUG
        Serial.printf("pktLength %u\r\n", pktLength);
#endif
        if (pktLength > sizeof(pktData)) {
          Serial.println("Packet length exceeds size of buffer! Data discarded");
          pktLengthAdjusted = sizeof(pktData);
        }
        else {
          pktLengthAdjusted = pktLength;
        }
        break;
      case 4: // Read pktLengthAdjusted bytes into pktData
        bytesRead = client.read(&pktData[bytesIn],
            minsize(sizeof(pktData), pktLengthAdjusted) - bytesIn);
        bytesIn += bytesRead;
        if (bytesIn >= pktLengthAdjusted) {
          if ((pktCommand == 0) && (pktChannel <= 1)) {
            int i;
            uint8_t *pixrgb;
            pixrgb = pktData;
            for (i = 0; i < minsize((pktLengthAdjusted / 3), NUM_LEDS); i++) {
              leds[i].setRGB(*pixrgb++, *pixrgb++, *pixrgb++);
            }
            // Display only the first frame in this cycle. Buffered frames
            // are discarded.
            if (frame_count == 0) {
#if OSCDEBUG
              Serial.print("=");
              unsigned long startMicros = micros();
#endif
#if OSCDEBUG
              Serial.printf("%lu\r\n", micros() - startMicros);
#endif
            }
            else {
              frame_discard++;
            }
            frame_count++;
          }
          if (pktLength == pktLengthAdjusted)
            packetParse = 0;
          else
            packetParse++;
        }
        break;
      default:  // Discard data that does not fit in pktData
        bytesRead = client.read(pktData, pktLength - bytesIn);
        bytesIn += bytesRead;
        if (bytesIn >= pktLength) {
          packetParse = 0;
        }
        break;
    }
  }
#if OSCDEBUG
  if (frame_discard) {
    Serial.printf("discard %u\r\n", frame_discard);
  }
#endif
}
*/
uint8_t retryCount = 0;
void WiFi_Handle(const char* ssid, const char* password) {
//  clientEvent();
  //WiFi_BPMEvent();
#ifdef UNIT_TYPE_CORSET
  if (disconnected) {
    unsigned long connectTime = millis() - connecting;
    if (WiFi.status() == WL_CONNECTED) {
      // we're connected!
      disconnected = false;
      Serial.print("Connected with IP address: ");
      Serial.println(WiFi.localIP());
    } else if (connectTime > 20000) {
      connecting = 0;
      Serial.println("Connecting timed out...reconnecting");
      retryCount++;
      if (retryCount >= 5) {
        hasScan = false;
        retryCount = 0;
      }
      WiFi_Connect(ssid, password);
    }
  } else if (WiFi.status() != WL_CONNECTED) {
    disconnected = true;
    Serial.println("Connection lost...reconnecting");
    WiFi_Connect(ssid, password);
  }
#endif
}
