#ifndef _WIFI_H
#define _WIFI_H
#include <ESP8266mDNS.h>
#include "LED_Config.h"

void WiFi_Setup(const char* ssid, const char* password);

#ifdef UNIT_TYPE_CORSET
void WiFi_Connect(const char* ssid, const char* password);
#endif

#ifdef UNIT_TYPE_SUIT
void WiFi_AP(const char* ssid, const char* password);
#endif
void WiFi_Handle(const char* ssid, const char* password);

extern MDNSResponder mdns;
extern bool hasMDNS;
extern bool disconnected;
extern unsigned long connecting;

#endif // _WIFI_H
