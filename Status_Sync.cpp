#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "WiFi.h"
#include "BPM.h"
#include "LED_Config.h"
#include "Status_Sync.h"
#include "Palettes.h"
#include "LED_Animations.h"

WiFiUDP udp;

#define STATUS_PORT 4211
#define STATUS_TCP_PORT 4210

#ifdef UNIT_TYPE_CORSET
    uint8_t bpm;
    uint32_t bpmOffset;
    uint8_t palette;
    uint8_t anim;
    uint8_t hue;
    uint8_t hOffset;
    uint16_t rSeed;
    uint8_t caleido;
    WiFiClient statusClient;

    void Status_Sync_Connect_Client() {
        if (statusClient.connect(WiFi.gatewayIP(), STATUS_TCP_PORT)) {
            Serial.print("Connected to BPM host at : ");
            Serial.println(WiFi.gatewayIP().toString());
        }
    }

    void Status_Sync_Setup() {
        // setup BPM UDP listener
        udp.begin(STATUS_PORT);
        Serial.print("UDP BPM listener started at : ");
        Serial.println(STATUS_PORT);
    }

    #define BPM_OFFSET 6
    void Status_Sync_Handle() {
        int packetSize = udp.parsePacket();
        do {
            if (packetSize) {
                udp.read(&bpm, 1);
                udp.read((uint8_t*)&bpmOffset, 4);
                setBeat(bpm, bpmOffset - BPM_OFFSET);
                udp.read(&palette, 1);
                setPalette(palette);
                udp.read(&anim, 1);
                udp.read((uint8_t*)&rSeed, 2);
                udp.read(&hue, 1);
                udp.read(&hOffset, 1);
                // if we're changing anim, reset the beat just in case things have slipped
                if (anim != gCurrentPatternNumber) {
                    globalBPM = 0;
                    setBeat(bpm, bpmOffset - BPM_OFFSET);
                }
                setPattern(anim, rSeed, hue, hOffset);
                udp.read(&caleido, 1);
                caleidoscope = caleido;
            }
        } while (packetSize = udp.parsePacket());
    }
#endif // UNIT_TYPE_CORSET



#ifdef UNIT_TYPE_SUIT
    WiFiServer statusServer(STATUS_TCP_PORT);
    #define MAX_CLIENTS 2
    IPAddress statusAddresses[MAX_CLIENTS] = {
        CLIENT_IP_1,
        CLIENT_IP_2
    };

    void Status_Sync_Send_Update_Addr(IPAddress addr, uint8_t bpm, uint32_t bpmOffset, uint8_t palette, uint8_t anim, uint16_t randomSeed, uint8_t hue, uint8_t hueOffset, uint8_t caleido) {
        if (addr == INADDR_NONE) {
            return;
        }
        udp.beginPacket(addr, STATUS_PORT);
        udp.write(bpm);
        udp.write((uint8_t*)&bpmOffset, 4);
        udp.write(palette);
        udp.write(anim);
        udp.write((uint8_t*)&randomSeed, 2);
        udp.write(hue);
        udp.write(hueOffset);
        udp.write(caleido);
        udp.endPacket();
    }
    void Status_Sync_Send_Update() {
        uint8_t bpm = globalBPM;
        uint32_t bpmOffset = millis() - prevBeat;
        uint8_t palette = gCurrentPaletteNumber;
        uint8_t anim = gCurrentPatternNumber;
        uint16_t randomSeed = random16_get_seed();
        uint8_t hue = gHue;
        uint8_t hOffset = hueOffset;
        for (uint8_t i=0; i < MAX_CLIENTS; i++) {
            Status_Sync_Send_Update_Addr(statusAddresses[i], bpm, bpmOffset, palette, anim, randomSeed, hue, hOffset, caleidoscope);
        }
    }

    void Status_Sync_Setup() {
        // its the suit, setup a TCP port to listen for incoming clients        
        /*if (hasMDNS) {
            mdns.addService("bpm", "tcp", STATUS_TCP_PORT);
        }*/
        statusServer.begin();
        Serial.print("Status server setup on ");
        Serial.println(STATUS_TCP_PORT);
    }

    void Status_Sync_Handle() {
        WiFiClient statusClient = statusServer.available();
        if (statusClient) {
            Serial.print("Connected : ");
            Serial.println(statusClient.remoteIP());
        }
        // send current status updates at 2hz
        EVERY_N_MILLISECONDS(30) {
            Status_Sync_Send_Update();
        }
    }
#endif // UNIT_TYPE_SUIT