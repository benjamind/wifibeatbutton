#include "Arduino.h"
#include "LED_Animations.h"

uint8_t numTargets = 4;

void Animation_Blocks() {
  EVERY_N_MILLISECONDS(8) {
    fadeToBlackBy( leds, NUM_LEDS, 30);
  }

  if (caleidoscope != 0) {
    numTargets = 2;
  } else {
    numTargets = 4;
  }

  // on the beat transition....
  if (isBeat()) {
    // pick new columns
    uint8_t target[4];
    for (uint8_t i=0; i < numTargets; i++) {
      target[i] = random8(2*numTargets);
    }
    // flash them on
    for (uint8_t col = 0; col < numTargets; col++) {      
      CRGB color = ColorFromPalette( currentPalette, gHue + /*(32*col) + */ (16*target[col]), 255);
      uint8_t colX = col*4;
      uint8_t rowY = target[col]*2;
      // draw the rectangles
      for (uint8_t x = colX; x < colX + 4; x++) {
        for (uint8_t y = rowY; y < rowY + 2; y++) {
          for (uint8_t p = 0; p < NUM_STRIPS; p++) {
            leds[XY(p, x,y)] = color;
          }
        }
      }
    }
  }
}
