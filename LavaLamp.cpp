#include "Arduino.h"
#include "LED_Animations.h"

void LavaLamp1() {  
  colorshift = 0;
  x[0] = beatsin16(scale8(globalBPM-40, 10), 200, 64000);
  uint8_t beat =  beat8(globalBPM, prevBeat);
  y[0] += beat;
  z[0] = 7000;
  scale_x[0] = 6000;
  scale_y[0] = 8000;
  FillNoise(0); 

  x[1] = beatsin16(scale8(globalBPM-40, 8), 200, 64000);
  y[1] += 130;
  z[1] = 7000;
  scale_x[1] = 12000;
  scale_y[1] = 16000;
  FillNoise(1);   

  x[2] = beatsin16(scale8(globalBPM-40, 20), 200, 6400);
  //y[2] += 1000;
  y[2] += 40*scale8(beat, 100) ;
  z[2] = 3000;
  scale_x[2] = 14000;
  scale_y[2] = 16000;
  FillNoise(2);

  noisesmoothing = 200;

  for (uint8_t p = 0; p < NUM_STRIPS; p++) {
    MergeMethod1(p, 2);
  }
}
