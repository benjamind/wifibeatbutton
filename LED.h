#ifndef _LED_H
#define _LED_H

#define FASTLED_ESP8266_RAW_PIN_ORDER
#define FASTLED_ALLOW_INTERRUPTS 1
#define INTERRUPT_THRESHOLD 1
#define FASTLED_INTERRUPT_RETRY_COUNT 0
#include "FastLED.h"
#include "LED_Config.h"
#include "LED_Animations.h"


////////////////////////////////////////////////////////////////////////////////
// LEDS
////////////////////////////////////////////////////////////////////////////////


extern CRGB leds[NUM_LEDS];

#define LED_TYPE    WS2812
#define COLOR_ORDER GRB
#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

#define BRIGHTNESS          128
#define DIMM_BRIGHTNESS     96
#define FRAMES_PER_SECOND  120

void LED_Setup();

void LED_Handle();

#endif // _LED_H
