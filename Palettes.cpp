#include "Arduino.h"
#include "Palettes.h"
#include "LED_Config.h"
#include "Status_Sync.h"

CRGBPalette16 currentPalette ( CRGB::Black );
CRGBPalette16 targetPalette ( RainbowColors_p );

uint8_t gCurrentPaletteNumber = 0; // Index number of which palette is current

void FillFromPalette( CRGB* leds, uint16_t numLEDS, uint8_t colorIndex, uint8_t brightness, uint8_t deltaHue) {
  for ( int i=0; i < numLEDS; i++) {
    leds[i] = ColorFromPalette( currentPalette, colorIndex + i*deltaHue, brightness);
  }
}

void Palette_Handle() {  
  // update palette
  uint8_t maxChanges = 24; //24;
  nblendPaletteTowardPalette( currentPalette, targetPalette, maxChanges);
#ifdef UNIT_TYPE_SUIT
  EVERY_N_SECONDS( 30 ) { changePalette(); } // change palettes periodically
#endif
}


// Gradient palette "aquamarinemermaid_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/aquamarinemermaid.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 20 bytes of program space.

DEFINE_GRADIENT_PALETTE( aquamarinemermaid_gp ) {
    0,  23,  4, 32,
   63,  98, 31, 52,
  127, 224,138, 24,
  191,   7, 55,164,
  255,  23,  4, 32};

// Gradient palette "otis_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/otis.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 16 bytes of program space.

DEFINE_GRADIENT_PALETTE( otis_gp ) {
    0,  26,  1, 89,
  127,  17,193,  0,
  216,   0, 34, 98,
  255,   0, 34, 98};
  
// Gradient palette "scoutie_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/scoutie.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 16 bytes of program space.

DEFINE_GRADIENT_PALETTE( scoutie_gp ) {
    0, 255,156,  0,
  127,   0,195, 18,
  216,   1,  0, 39,
  255,   1,  0, 39};
// Gradient palette "nrwc_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/wkp/tubs/tn/nrwc.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 44 bytes of program space.

DEFINE_GRADIENT_PALETTE( nrwc_gp ) {
    0,   1,  1,  1,
   25,   4,  8,  1,
   51,   1, 11,  2,
   76,   4, 36,  9,
  102,   6, 66, 18,
  127,  27, 95, 23,
  153,  82,127, 31,
  178, 197,171, 40,
  204, 133,100, 19,
  229,  97, 48,  6,
  255, 163, 55,  7};
  
// Gradient palette "DEM_poster_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/td/tn/DEM_poster.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 32 bytes of program space.

DEFINE_GRADIENT_PALETTE( DEM_poster_gp ) {
    0,   0, 30, 10,
    2,   1, 50,  3,
   26, 199,175, 42,
   62,  77, 13,  0,
   88,  73,  0,  0,
  145,  28, 40, 31,
  208, 255,255,255,
  255, 255,255,255};
  
// Gradient palette "elevation_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/grass/tn/elevation.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 24 bytes of program space.

DEFINE_GRADIENT_PALETTE( elevation_gp ) {
    0,   0,135,123,
   51,   0,255,  0,
  102, 255,255,  0,
  153, 255, 55,  0,
  204, 120, 55,  7,
  255,   1,  1,  1};
  
// Gradient palette "bhw1_01_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/bhw/bhw1/tn/bhw1_01.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 12 bytes of program space.

DEFINE_GRADIENT_PALETTE( bhw1_01_gp ) {
    0, 227,101,  3,
  117, 194, 18, 19,
  255,  92,  8,192};
  
// Gradient palette "purplefly_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/purplefly.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 16 bytes of program space.

DEFINE_GRADIENT_PALETTE( purplefly_gp ) {
    0,   0,  0,  0,
   63, 239,  0,122,
  191, 252,255, 78,
  255,   0,  0,  0};

// Gradient palette "GMT_ocean_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/gmt/tn/GMT_ocean.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 36 bytes of program space.

DEFINE_GRADIENT_PALETTE( GMT_ocean_gp ) {
    0,   0,  0,  0,
   31,   0,  1,  1,
   63,   0,  1,  4,
   95,   0, 19, 42,
  127,   0, 79,138,
  159,  15,144,112,
  191,  91,233, 89,
  223, 155,244,158,
  255, 242,255,255};

// Gradient palette "bhw4_099_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/bhw/bhw4/tn/bhw4_099.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 48 bytes of program space.

DEFINE_GRADIENT_PALETTE( bhw4_099_gp ) {
    0, 249, 32,145,
   28, 208,  1,  7,
   43, 249,  1, 19,
   56, 126,152, 10,
   73, 234, 23, 84,
   89, 224, 45,119,
  107, 232,127,158,
  127, 244, 13, 89,
  150, 188,  6, 52,
  175, 177, 70, 14,
  221, 194,  1,  8,
  255, 112,  0,  1};
  
// Gradient palette "bhw4_009_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/bhw/bhw4/tn/bhw4_009.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 32 bytes of program space.

DEFINE_GRADIENT_PALETTE( bhw4_009_gp ) {
    0,  66,186,192,
   43,   1, 22, 71,
   79,   2,104,142,
  117,  66,186,192,
  147,   2,104,142,
  186,   1, 22, 71,
  224,   2,104,142,
  255,   4, 27, 28};
// Gradient palette "bhw3_13_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/bhw/bhw3/tn/bhw3_13.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 24 bytes of program space.

DEFINE_GRADIENT_PALETTE( bhw3_13_gp ) {
    0,   3,  6, 72,
   38,  12, 50,188,
  109, 217, 35,  1,
  135, 242,175, 12,
  178, 161, 32, 87,
  255,  24,  6,108};
// Gradient palette "bhw3_61_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/bhw/bhw3/tn/bhw3_61.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 24 bytes of program space.

DEFINE_GRADIENT_PALETTE( bhw3_61_gp ) {
    0,  14,  1, 27,
   48,  17,  1, 88,
  104,   1, 88,156,
  160,   1, 54, 42,
  219,   9,235, 52,
  255, 139,235,233};
// Gradient palette "bhw3_52_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/bhw/bhw3/tn/bhw3_52.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 28 bytes of program space.

DEFINE_GRADIENT_PALETTE( bhw3_52_gp ) {
    0,  31,  1, 27,
   45,  34,  1, 16,
   99, 137,  5,  9,
  132, 213,128, 10,
  175, 199, 22,  1,
  201, 199,  9,  6,
  255,   1,  0,  1};
// Gradient palette "bhw2_xmas_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/bhw/bhw2/tn/bhw2_xmas.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 48 bytes of program space.

DEFINE_GRADIENT_PALETTE( bhw2_xmas_gp ) {
    0,   0, 12,  0,
   40,   0, 55,  0,
   66,   1,117,  2,
   77,   1, 84,  1,
   81,   0, 55,  0,
  119,   0, 12,  0,
  153,  42,  0,  0,
  181, 121,  0,  0,
  204, 255, 12,  8,
  224, 121,  0,  0,
  244,  42,  0,  0,
  255,  42,  0,  0};
// Gradient palette "bhw2_22_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/bhw/bhw2/tn/bhw2_22.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 20 bytes of program space.

DEFINE_GRADIENT_PALETTE( bhw2_22_gp ) {
    0,   0,  0,  0,
   99, 227,  1,  1,
  130, 249,199, 95,
  155, 227,  1,  1,
  255,   0,  0,  0};


CRGBPalette16 gPalettes[] = { RainbowColors_p, scoutie_gp, otis_gp, aquamarinemermaid_gp, bhw1_01_gp, nrwc_gp, DEM_poster_gp, elevation_gp, bhw1_01_gp, purplefly_gp, GMT_ocean_gp, bhw4_099_gp, bhw4_009_gp, bhw3_13_gp, bhw3_61_gp, bhw3_52_gp, bhw2_xmas_gp, bhw2_22_gp, LavaColors_p, CloudColors_p };

void changePalette()
{
  uint8_t paletteNumber = random8(0, ARRAY_SIZE(gPalettes));
  setPalette(paletteNumber);
}

void setPalette(uint8_t paletteNumber) {
  if (paletteNumber != gCurrentPaletteNumber && paletteNumber < ARRAY_SIZE(gPalettes)) {
    gCurrentPaletteNumber = paletteNumber;
    targetPalette = gPalettes[gCurrentPaletteNumber];
#ifdef UNIT_TYPE_SUIT
    Status_Sync_Send_Update();
#endif
    Serial.print("Palette = ");
    Serial.println(gCurrentPaletteNumber);
  }
}