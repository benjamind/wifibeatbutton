#ifndef BPM_H
#define BPM_H

#define BPM_MODE_FLASH 8
#define BPM_MODE_NONE 0

extern uint8_t beatMode;

extern uint32_t prevBeat;
extern uint16_t globalBPM; 
extern uint16_t bpmMillis;
bool isBeat(uint8_t win = 0);
void tapBeat();
void setBeat(uint8_t bpm, uint32_t bpmOffset);

#endif // BPM_H