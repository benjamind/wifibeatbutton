#include "Arduino.h";

#include "LED.h";
#include "LED_Animations.h";

#if defined(FASTLED_VERSION) && (FASTLED_VERSION < 3001000)
#warning "Requires FastLED 3.1 or later; check github for latest code."
#endif

CRGB leds[NUM_LEDS];
uint8_t globalBrightness = BRIGHTNESS;

void LED_Setup() {  

  // parallel output
  FastLED.addLeds<WS2811_PORTA,NUM_STRIPS,COLOR_ORDER>(leds, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);

  // separate controllers
  /*FastLED.addLeds<LED_TYPE, 15, COLOR_ORDER>(leds, 0, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
  FastLED.addLeds<LED_TYPE, 22, COLOR_ORDER>(leds, NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
  FastLED.addLeds<LED_TYPE, 23, COLOR_ORDER>(leds, NUM_LEDS_PER_STRIP*2, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
  FastLED.addLeds<LED_TYPE, 9, COLOR_ORDER>(leds, NUM_LEDS_PER_STRIP*3, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);*/
  
  FastLED.setMaxPowerInVoltsAndMilliamps(5,500 * NUM_STRIPS);
  // set master brightness control
  FastLED.setBrightness(globalBrightness);

  LED_Animations_Setup();  
  FastLED.show(); // clear to black
}

void LED_Handle() {
  // send the 'leds' array out to the actual LED strip
  // insert a delay to keep the framerate modest
  LED_Animations_Handle();
  FastLED.show();  

  //FastLED.delay(1000/FRAMES_PER_SECOND); 
  /*FastLED.countFPS();
  EVERY_N_MILLISECONDS(500) {
    Serial.print(millis());
    Serial.print(": FPS:");
    Serial.println(FastLED.getFPS());
    Serial.print("Frame cnt: ");
    Serial.println(_frame_cnt);
    Serial.print("Retry cnt: ");
    Serial.println(_retry_cnt);
  }*/
}
