#include "Arduino.h"
#include "LED.h"
#include "LED_Animations.h"
#include "FastLED.h"
#include "Palettes.h"
#include "Status_Sync.h"
#include "LED_Config.h"
#include "WiFi.h"

uint8_t countX = 0;
uint8_t countY = 0;
uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns
uint32_t moveTil = 0L;

void LED_Animations_Setup() {
  Noise_Setup();
}

SimplePatternList gPatterns = {
  Animation_Blocks, 
  Animation_RandomWalks, 
  Animation_Bubbles1, 
  Animation_MirroredNoise, 
  Animation_BPMPanels, 
  Animation_CylonPanels
};

void LED_Animations_Handle() {
  gPatterns[gCurrentPatternNumber]();
  ApplyCaleidoscope();
/*
#ifdef UNIT_TYPE_CORSET
    int CentreX = MATRIX_WIDTH / 2;
    int CentreY = MATRIX_HEIGHT / 2;
    for(int x = 0; x < MATRIX_WIDTH ; x++) {
      for(int y = x; y < MATRIX_HEIGHT; y++) {
        leds[XY( 0, y, x )] = leds[XY( 0, x, y )]; 
      }
    }
#endif
*/
  Palette_Handle();
  // do some periodic updates
#ifdef UNIT_TYPE_CORSET
  if (disconnected) {
    EVERY_N_SECONDS( 60 ) { nextPattern(); } // change patterns periodically
    EVERY_N_SECONDS(10) { hueOffset = random8(10, 15); }
  }
#endif
#ifdef UNIT_TYPE_SUIT
  EVERY_N_SECONDS( 60 ) { nextPattern(); } // change patterns periodically
  EVERY_N_SECONDS(10) { hueOffset = random8(10, 15); }
#endif
  EVERY_N_MILLISECONDS( 20 ) { gHue++; } // slowly cycle the "base color" through the rainbow
}

uint8_t allowedCaleido[] = {
  // Blocks - no rotate
  CALEIDO_VERT_MIRROR | CALEIDO_HORZ_MIRROR | CALEIDO_SCOPE2,
  // Random walks
  CALEIDO_ROTATE | CALEIDO_VERT_MIRROR | CALEIDO_HORZ_MIRROR | CALEIDO_SCOPE1 | CALEIDO_SCOPE2 | CALEIDO_SCOPE3,
  // Bubbles1
  CALEIDO_ROTATE | CALEIDO_VERT_MIRROR | CALEIDO_HORZ_MIRROR | CALEIDO_SCOPE1 | CALEIDO_SCOPE2 | CALEIDO_SCOPE3,
  // MirroredNoise
  CALEIDO_ROTATE | CALEIDO_VERT_MIRROR | CALEIDO_HORZ_MIRROR | CALEIDO_SCOPE1 | CALEIDO_SCOPE2 | CALEIDO_SCOPE3,
  // BPM Panels
  CALEIDO_VERT_MIRROR | CALEIDO_HORZ_MIRROR | CALEIDO_SCOPE2 | CALEIDO_SCOPE3,
  // Cylon Panels
  CALEIDO_VERT_MIRROR | CALEIDO_HORZ_MIRROR | CALEIDO_SCOPE2 | CALEIDO_SCOPE3,
};

void PickCaleidoscope() {
  int i = 0;
  while (i < 10) {
    uint8_t index = random8(6);
    if (index == 0) {
      caleidoscope = 0;
      return;
    }
    uint8_t mask = 1 << index;
    /*Serial.print("Mask : ");
    Serial.println(mask);
    Serial.print("allowedCaleido: ");
    Serial.println(allowedCaleido[gCurrentPatternNumber]);*/
    if ((allowedCaleido[gCurrentPatternNumber] & mask) == mask) {
      caleidoscope = mask;
      Serial.print("Caleidoscope: ");
      Serial.println(caleidoscope);
      return;
    }
    i++;
  }
}

uint8_t beatBrightness(uint8_t minBrightness, uint8_t maxBrightness) {
  uint8_t beat =  beat8(globalBPM, prevBeat);
  uint8_t wave = squarewave8(beat, 16);
  uint8_t rangewidth = maxBrightness - minBrightness;
  uint8_t scaledBrightness = scale8(wave, rangewidth);
  uint8_t brightness = minBrightness + scaledBrightness;
  return brightness;
}

void randomPattern()
{
  uint8_t anim = random8(0, ARRAY_SIZE(gPatterns));
  setPattern(anim, random16_get_seed(), gHue, hueOffset);
}

void nextPattern()
{
  // add one to the current pattern number, and wrap around at the end
  uint8_t anim = (gCurrentPatternNumber + 1) % ARRAY_SIZE( gPatterns);
  setPattern(anim, random16_get_seed(), gHue, hueOffset);
}

#ifdef UNIT_TYPE_SUIT
void setPattern(uint8_t anim, uint16_t randomSeed, uint8_t hue, uint8_t hOffset) {
  if (anim != gCurrentPatternNumber && anim < ARRAY_SIZE(gPatterns)) {
    gCurrentPatternNumber = anim;
    Serial.println("Broadcasting pattern");
    PickCaleidoscope();
    //random16_set_seed(randomSeed);
    gHue = hue;
    gHueOffset = hOffset; 
    Status_Sync_Send_Update();
    Serial.print("Animation = ");
    Serial.println(gCurrentPatternNumber);
  }
}
#endif
#ifdef UNIT_TYPE_CORSET
void setPattern(uint8_t anim, uint16_t randomSeed, uint8_t hue, uint8_t hOffset) {
  if (anim != gCurrentPatternNumber && anim < ARRAY_SIZE(gPatterns)) {
    random16_set_seed(randomSeed);
    gCurrentPatternNumber = anim;
    gHue = hue;
    gHueOffset = hOffset; 
    Serial.print("Animation = ");
    Serial.println(gCurrentPatternNumber);
  }
}
#endif

void debugHue() {
  
  fadeToBlackBy( leds, NUM_LEDS, 10);
  /*for (uint8_t p = 0; p < NUM_STRIPS; p++) {
    //for (uint8_t i = 0; i < countX; i++) {
      leds[POS(p, countX)] = CRGB::Yellow;
    //}
  }*/
  
  /*for (uint8_t p = 0; p < NUM_STRIPS; p++) {
    //for (uint8_t x = 0; x < countX; x++) {
      for (uint8_t y = 0; y < countY; y++) {
        leds[XY(p, countX,y)] = CRGB::Yellow;
      }
    //}
  }*/
  for (uint8_t x = 0; x < MATRIX_WIDTH; x++) {
    for (uint8_t p = 0; p < NUM_STRIPS; p++) {
      leds[XY(p, x, countY)]+= ColorFromPalette(currentPalette, 32*p, 255);
    }
  }
  EVERY_N_MILLISECONDS(300) {
    countY++;
    if (countY > 16) {
      countY = 0;
    }
    countX++;
    if (countX > 16) {
      countX = 0;
    }
  }
}
