#include "Arduino.h"
#include "BPM.h"
#include "FastLED.h"
#include "LED_Config.h"
#include "Status_Sync.h"

// define the variables
uint16_t globalBPM = 60; 
uint8_t beatMode = BPM_MODE_FLASH;

uint32_t prevBeat = 0L;
uint32_t sum = 0L;
uint16_t nBeats = 0;
uint16_t bpmMillis = (60*1000) / globalBPM;
bool beatHandled = false;
uint8_t beatWindow = 32;

bool isBeat(uint8_t win) {  
  if (win != 0 && beatWindow != win) {
    beatWindow = win;
  } else {
    beatWindow = 32;
  }
  uint8_t beat = beat8(globalBPM, prevBeat);
  uint8_t wave = squarewave8(beat, beatWindow);
  
  if (wave > 0 && !beatHandled) {
    beatHandled = true;
    return true;
  } else if (wave == 0 && beatHandled) {
    beatHandled = false;
    return false;
  }
  return false;
}

void setBeat(uint8_t bpm, uint32_t bpmOffset) {
  if (bpm != globalBPM) {
    globalBPM = bpm;
    bpmMillis = (60*1000) / globalBPM;
    prevBeat = millis() - bpmOffset; // fudge the beat offset a little
    Serial.print("BPM = ");
    Serial.println(globalBPM);
  }
}

void tapBeat() {
  nBeats++;
  unsigned long t = millis();
  unsigned long diff = t - prevBeat;
  if (diff > 4000) {
    // after 4 seconds, reset the beat on the next push
    sum = 0;
    nBeats = 0;
  } else {
    sum += (60*1000) / (diff);
    globalBPM = (sum / nBeats);
    if ( globalBPM > 255 ) {
      globalBPM = 255;
    }
    bpmMillis = (60*1000) / globalBPM;
#ifdef UNIT_TYPE_SUIT
    Status_Sync_Send_Update();
#endif
    Serial.print("BPM = ");
    Serial.println(globalBPM);
  }
  prevBeat = t;
}