#include "Arduino.h"
#include "LED_Animations.h"
#include "Graphics.h"
#include "Caleidoscope.h"

void Animation_Ghost() {
    EVERY_N_MILLISECONDS(8) {
        MoveOscillators();
        //if (random(255) < 120)
        for (uint8_t p; p < NUM_STRIPS; p++) {
            leds[XY(p, (pa[2] + pa[0] + pa[1]) / 3, (pa[1] + pa[3] + pa[2]) / 3)] = CRGB(255, 255, 255);
        }

        SpiralStream(MATRIX_WIDTH / 2 - 5, MATRIX_HEIGHT / 2 - 5, 5, 160);
        SpiralStream(MATRIX_WIDTH / 2 + 5, MATRIX_HEIGHT / 2 + 5, 5, 160);
    }
    EVERY_N_MILLISECONDS(15) {
        HorizontalStream(50);
    }
}
void Animation_SlowMandala() {
    EVERY_N_MILLISECONDS(8) {
        for (uint8_t i = 0; i < MATRIX_WIDTH / 2; i++) {
            for (uint8_t j = 0; j < MATRIX_HEIGHT / 2; j++) {
                for (uint8_t p = 0; p < NUM_STRIPS; p++) {
                    leds[XY(p, i, j)] = CRGB(255, 0, 0);
                    Caleidoscope1(p);
                }
                SpiralStream(8, 8, 8, 127);
            }
        }
    }
}
void Animation_Dots1() {
    EVERY_N_MILLISECONDS(8) {
        // advance the snake positions randomly
        osci[0] = osci[0] + 3;
        osci[1] = osci[1] + 5;
        osci[2] = osci[2] + 3;
        osci[3] = osci[3] + 4;
        
        for (int i = 0; i < 4; i++) {
            pa[i] = map(beatsin8(globalBPM, 0, osci[i]), 0, osci[i], 0, MATRIX_WIDTH - 1); //why? to keep the result in the range of 0-MATRIX_WIDTH (matrix size)
        }
    }

    //2 lissajous dots red
    for (uint8_t p = 0; p < NUM_STRIPS; p++) {
        leds[XY(p, pa[0], pa[1])] = ColorFromPalette(currentPalette, gHue, 30);
        leds[XY(p, pa[2], pa[3])] = ColorFromPalette(currentPalette, gHue, 30);
    }
    //average of the coordinates in yellow
    Pixel((pa[2] + pa[0]) / 2, (pa[1] + pa[3]) / 2, ColorFromPalette(currentPalette, gHue + 128, 5));

    EVERY_N_MILLISECONDS(20) {
        HorizontalStream(100);
    }
}
void Animation_Dots2() {
    uint8_t beat = beatsin8( globalBPM, 0, 3);
    uint8_t beat2 = beatsin8( globalBPM, 0, 4);
    uint8_t beat3 = beatsin8( globalBPM, 0, 5);
    bool onBeat = isBeat();
    EVERY_N_MILLISECONDS(8) {
        osci[0] = osci[0] + 3;
        osci[1] = osci[1] + 1;
        osci[2] = osci[2] + 2;
        osci[3] = osci[3] + 2;
        if (onBeat) {
            osci[0] = osci[0] + beat;
            osci[1] = osci[1] + beat2;
            osci[2] = osci[2] + beat3;
            osci[3] = osci[3] + beat;
        }
        for (int i = 0; i < 4; i++) {
            pa[i] = map(sin8(osci[i]), 0, 255, 0, MATRIX_WIDTH - 1); //why? to keep the result in the range of 0-MATRIX_WIDTH (matrix size)
        }
    }
    Pixel((pa[2] + pa[0] + pa[1]) / 3, (pa[1] + pa[3] + pa[2]) / 3, ColorFromPalette(currentPalette, gHue, onBeat ? 255 : 1));

    EVERY_N_MILLISECONDS(20) {
        DimAll(200);
    }
}
uint8_t particleX = 0;
uint8_t particleY = 0;
void Animation_AudioSpiral() {
    if (isBeat(32)) {
        moveTil = millis() + bpmMillis/4;
        particleX = random8(MATRIX_WIDTH);
        particleY = random8(MATRIX_HEIGHT);
    }

    SpiralStream(8, 8, 7, 180);
    SpiralStream(9, 9, 7, 180);
    /*SpiralStream(4, 4, 4, 122);
    SpiralStream(11, 11, 3, 122);*/

    for (uint8_t p = 0; p < NUM_STRIPS; p++) {
        leds[XY(p, 0, 0)] = ColorFromPalette( currentPalette, gHue + 128, 255);
    }
    if (millis() > moveTil) {   
        for (uint8_t p = 0; p < NUM_STRIPS; p++) {
            leds[XY(p, particleX, particleY)] = ColorFromPalette( currentPalette, gHue, 255);
            //leds[XY(p, x-1, y-1)] = ColorFromPalette( currentPalette, h, 255);
            //leds[XY(p, x+1, y+1)] = ColorFromPalette( currentPalette, h, 255);
        }
    }

    EVERY_N_MILLISECONDS(50) {
        DimAll(254);
    }
}