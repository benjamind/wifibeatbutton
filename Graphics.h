#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "FastLED.h"

void CLS();
void ConstrainedMapping(uint8_t index, byte layer, byte lower_limit, byte upper_limit, byte colorrepeat);
void MergeMethod1(uint8_t index, byte colorrepeat);
void CrossMapping(uint8_t index, byte colorrepeat, byte limit);

uint16_t XY(uint8_t index, uint8_t x, uint8_t y);
uint16_t POS(uint8_t index, uint8_t i);

// the oscillators: linear ramps 0-255
extern byte osci[4];
// sin8(osci) swinging between 0 to WIDTH - 1
extern byte pa[4];

void SpiralStream(int x, int y, int r, byte dimm);
void Line(uint8_t panel, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, byte color);
void Pixel(int x, int y, byte color);
void HorizontalStream(byte scale);
void VerticalStream(byte scale);
void VerticalMove();
void Copy(byte x0, byte y0, byte x1, byte y1, byte x2, byte y2);
void RotateTriangle();
void MirrorTriangle();
void RainbowTriangle();
void MoveOscillators();
void DimAll(byte value);
#endif // GRAPHICS_H