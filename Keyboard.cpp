#include "Arduino.h"
#include <Bounce2.h>
#include "LED_Animations.h"
#include "Keyboard.h"

Bounce key1 = Bounce();  // 10 ms debounce
Bounce key2 = Bounce();  // 10 ms debounce
Bounce key3 = Bounce();  // 10 ms debounce
Bounce key4 = Bounce();  // 10 ms debounce
Bounce key5 = Bounce();  // 10 ms debounce
Bounce key6 = Bounce();  // 10 ms debounce

void Keyboard_Setup() {
 
  // setup input pins
  pinMode(KEYBOARD_1, INPUT_PULLUP);
  digitalWrite(KEYBOARD_1, HIGH);
  key1.attach(KEYBOARD_1);
  key1.interval(10);
   /*
  pinMode(KEYBOARD_2, INPUT_PULLUP);
  digitalWrite(KEYBOARD_2, HIGH);  
  key2.attach(KEYBOARD_2);
  key2.interval(10);*/
  
  pinMode(KEYBOARD_3, INPUT_PULLUP);
  digitalWrite(KEYBOARD_3, HIGH);
  key3.attach(KEYBOARD_3);
  key3.interval(10);
  /*
  pinMode(KEYBOARD_4, INPUT_PULLUP);
  digitalWrite(KEYBOARD_4, HIGH);
  key4.attach(KEYBOARD_4);
  key4.interval(10);
  
  pinMode(KEYBOARD_5, INPUT_PULLUP);
  digitalWrite(KEYBOARD_5, HIGH);
  key5.attach(KEYBOARD_5);
  key5.interval(10);*/
  
  pinMode(KEYBOARD_6, INPUT_PULLUP);
  digitalWrite(KEYBOARD_6, HIGH);
  key6.attach(KEYBOARD_6);
  key6.interval(10);
}

void Keyboard_Handle() {
  // bpm
  if (key3.update()) {
      if (key3.fell()) {
        Serial.println("Key3");
        nextPattern();
      }
  }
  // disable bpm flash
  /*if (key2.update()) {
      if (key2.fell()) {
        if (beatMode == BPM_MODE_NONE) {
          beatMode = BPM_MODE_FLASH;
          Serial.println("Flash Mode");
        } else {
          beatMode = BPM_MODE_NONE;
          // force brightness up to max
          FastLED.setBrightness(globalBrightness);
          Serial.println("Constant Mode");
        }
      }
  }  */ 
  if (key1.update()) {
    if (key1.fell()) {
      Serial.println("Key1");
      NextCaleidoscope();
    }
  }/*
  if (key4.update()) {
    if (key4.fell()) {
      Serial.println("Key4");
      ClearCaleidoscope();
    }
  }
  if (key5.update()) {
    if (key5.fell()) {
      Serial.println("Key5");
      changePalette();
    }
  }*/
  if (key6.update()) {
    if (key6.fell()) {
      Serial.println("Key6");
      tapBeat();
    }
  }
}