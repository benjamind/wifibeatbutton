#include "Arduino.h"
#include "LED_Animations.h"

namespace Snakes {
  typedef struct {
    byte x;
    byte y;
    byte lastX;
    byte lastY;
    int speed;
    byte col;
    byte panel;
  } Snake;
  
  void MoveSnake(Snake *snake, uint8_t maxWidth = MATRIX_WIDTH, uint8_t maxHeight = MATRIX_HEIGHT) {
    // pick an x and a y until we get one that is not lastX and lastY
    snake->lastX = snake->x;
    snake->lastY = snake->y;
    byte loops = 0;
    while (((snake->x == snake->lastX && snake->y == snake->lastY) ||
      leds[XY(0, snake->x, snake->y)].getLuma() > 32) &&
      loops < 32) {
      byte randDir = random(4);
      switch (randDir) {
        case 0:
          snake->x = snake->x + 1;
          snake->y = snake->y;
          break;
        case 1:
          snake->x = snake->x - 1;
          snake->y = snake->y;
          break;
        case 2:
          snake->x = snake->x;
          snake->y = snake->y + 1;
          break;
        case 3:
          snake->x = snake->x;
          snake->y = snake->y - 1;
          break;
      }
      if (snake->x == 255) {
        snake->x = maxWidth-1;
      } else if (snake->x >= maxWidth) {
        snake->x = 0;
      }
      if (snake->y == 255) {
        snake->y = maxHeight-1;
      } else if (snake->y >= maxHeight) {
        snake->y = 0;
      }
      loops++;
    }
  }
}

#define MAX_SNAKES 8
Snakes::Snake theSnakes[MAX_SNAKES];
bool initializedSnakes = false;
/*
 * Creates a random number of little snakes, that walk around the matrix never going over the edge
 * 
 */
void Animation_RandomWalks() {
  if (!initializedSnakes) {
    for (byte i = 0; i < MAX_SNAKES; i++) {
      theSnakes[i].x = random8(MATRIX_WIDTH);
      theSnakes[i].y = random8(MATRIX_HEIGHT);
      theSnakes[i].lastX = theSnakes[i].x;
      theSnakes[i].lastY = theSnakes[i].y;
      theSnakes[i].speed = 34;
      theSnakes[i].col = (255 / MAX_SNAKES) * i;
      Snakes::MoveSnake((Snakes::Snake*)&theSnakes[i]);
    }
    initializedSnakes = true;
  }
  uint8_t matrixWidth = MATRIX_WIDTH;
  uint8_t matrixHeight = MATRIX_HEIGHT;
  uint8_t numSnakes = MAX_SNAKES;
  if (caleidoscope != 0) {
    matrixWidth = MATRIX_WIDTH / 2;
    matrixHeight = MATRIX_HEIGHT / 2;
    numSnakes = 3;
  }

  // fade previous snakes a little bit
  EVERY_N_MILLISECONDS(8) {
    fadeToBlackBy( leds, NUM_LEDS, 20);
    if (isBeat(32)) {
      moveTil = millis() + bpmMillis/4;
    }
    // advance the snake positions randomly
    if (millis() < moveTil) {
      for (byte i = 0; i < numSnakes; i++) {
        Snakes::MoveSnake((Snakes::Snake*)&theSnakes[i], matrixWidth, matrixHeight);
      }
      // draw them all now
      for (byte i = 0; i < numSnakes; i++) {
        // draw all the snakes in their new positions  
        CRGB col = ColorFromPalette( currentPalette, (255 / numSnakes) * i, 255 );
        for (uint8_t p = 0; p < NUM_STRIPS; p++) {
          leds[XY(p, theSnakes[i].x,theSnakes[i].y)] = col;
        }
      }
    }  
  }
}
