#include "Arduino.h"
#include "LED_Animations.h"

void Animation_Bubbles1() {
  uint8_t beat =  beat8(globalBPM, prevBeat);
  uint8_t sinbeat = sin8(beat);
  noisesmoothing = 200;

  EVERY_N_MILLISECONDS(8) {
    x[0] = beatsin16(7);
    y[0] += 2000;
    z[0] = 14000;
    scale_x[0] = 12000;
    scale_y[0] = 12000;
  }
  
  FillNoise(0); 

  EVERY_N_MILLISECONDS(8) {
    x[1] = beatsin16(8);
    y[1] += 30*scale8(beat, 100) ;
    //y[1] += 3000;
    z[1] = 20000;
    scale_x[1] = 12000;
    scale_y[1] = 12000;
  }

  FillNoise(1); 

  CLS();

  for (uint8_t p = 0; p < NUM_STRIPS; p++) {
    ConstrainedMapping(p, 1, 0, 80+scale8(sinbeat, 64), 3);
  }
}
