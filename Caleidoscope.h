#ifndef CALEIDOSCOPE_H
#define CALEIDOSCOPE_H

////////////////////////////////////////////////////////////////////////////////
// Caleidoscope
////////////////////////////////////////////////////////////////////////////////
extern uint8_t caleidoscope;
#define NUM_CALEIDOSCOPES 7

#define CALEIDO_NONE   0
#define CALEIDO_ROTATE 1
#define CALEIDO_VERT_MIRROR 2
#define CALEIDO_HORZ_MIRROR 4
#define CALEIDO_SCOPE1 8
#define CALEIDO_SCOPE2 16
#define CALEIDO_SCOPE3 32
#define CALEIDO_MAX 64

void Caleido3();
void Caleidoscope1(uint8_t panel);
void Caleidoscope2(uint8_t panel);
void Caleidoscope3(uint8_t panel);
void Caleidoscope4(uint8_t panel);
void Caleidoscope5(uint8_t panel);
void CrossNoise();
void ApplyCaleidoscope();
void PickCaleidoscope();
void ClearCaleidoscope();
void ToggleCaleidoscope();
void NextCaleidoscope();

#endif
