#ifndef PALETTES_H
#define PALETTES_H

#include "FastLED.h"

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

////////////////////////////////////////////////////////////////////////////////
// Palettes
////////////////////////////////////////////////////////////////////////////////
extern CRGBPalette16 currentPalette;
extern CRGBPalette16 targetPalette;

extern uint8_t gCurrentPaletteNumber; // Index number of which palette is current
extern CRGBPalette16 gPalettes[];

void changePalette();
void setPalette(uint8_t paletteNumber);
void Palette_Handle();

void FillFromPalette( CRGB* leds, uint16_t numLEDS, uint8_t colorIndex, uint8_t brightness, uint8_t deltaHue=0);

#endif // PALETTES_H
