#include "Arduino.h"
#include "LED_Animations.h"

void Animation_MirroredNoise() {
  scale_x[0] = 15000;
  scale_y[0] = 15000;
  noisesmoothing = 200;
  
  // move within the noise space
  EVERY_N_MILLISECONDS(8) {
    if (isBeat(32)) {
      moveTil = millis() + bpmMillis/16;
    }
    if (millis() < moveTil) {
      x[0] += 3000;
      z[0] += 3000;
    } else {
      x[0] += 50;
      z[0] += 50;
    }
  }
  // calculate the noise array
  FillNoise(0);
  /*
  x[1] = beatsin16(8);
  y[1] += 40*scale8(globalBPM, 100) ;
  //y[1] += 3000;
  z[1] = 20000;
  scale_x[1] = 12000;
  scale_y[1] = 12000;
  FillNoise(1); */
  
  //currentPalette = RainbowStripeColors_p;

  //uint8_t brightness = beatBrightness(0, 255);
  for(int i = 0; i < MATRIX_WIDTH; i++) {
    for(int j = 0; j < MATRIX_HEIGHT; j++) {

      // map the noise values down
      uint16_t index = ( noise[0][i][j] + noise[0][MATRIX_WIDTH - 1 - i][j] );
      // assign a color from the HSV space
      CRGB color = ColorFromPalette( currentPalette, index + gHue, 255).fadeToBlackBy(index);

      for (uint8_t p = 0; p < NUM_STRIPS; p++) {
        leds[XY(p, i,j)] = color;
      }
    }
  }
}
