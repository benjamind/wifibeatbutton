#include "Arduino.h"
#include "Graphics.h"
#include "LED.h"
#include "Palettes.h"
#include "Noise.h"

// the oscillators: linear ramps 0-255
byte osci[4];
byte pa[4];
////////////////////////////////////////////////////////////////////////////////
// Graphics
////////////////////////////////////////////////////////////////////////////////
void MergeMethod1(uint8_t index, byte colorrepeat) { 
  for(uint8_t i = 0; i < MATRIX_WIDTH; i++) {
    for(uint8_t j = 0; j < MATRIX_HEIGHT; j++) {

      uint8_t color = ( ( noise[0][i][j] )
        + ( noise[1][i][j] )
        + ( noise[2][i][j] ) )
        / 3; 

      // layer 2 gives the brightness  
      uint8_t   bri = (noise[2][i][j]);

      // assign a color depending on the actual palette
      CRGB pixel = ColorFromPalette( currentPalette, colorrepeat * (color + colorshift), bri );

      leds[XY(index, i,j)] = pixel;
    }
  }
}

void CrossMapping(uint8_t index, byte colorrepeat, byte limit) { 
  for(uint8_t i = 0; i < MATRIX_WIDTH; i++) {
    for(uint8_t j = 0; j < MATRIX_HEIGHT; j++) {

      uint8_t color1 = noise[0][i][j];
      uint8_t color2 = noise[0][j][i];

      CRGB pixel;

      if (color1 > limit) {
        pixel = ColorFromPalette( currentPalette, colorrepeat * (color1 + colorshift), color2 );
      }
      else {
        pixel = ColorFromPalette( currentPalette, colorrepeat * (color2 + colorshift + 128), color1 );
      }
      leds[XY(index, i,j)] = pixel;
    }
  }
}
void CLS()  
{
  for(int i = 0; i < NUM_LEDS; i++) {
    leds[i] = 0;
  }
}
void ConstrainedMapping(uint8_t index, byte layer, byte lower_limit, byte upper_limit, byte colorrepeat) {

  for(uint8_t i = 0; i < MATRIX_WIDTH; i++) {
    for(uint8_t j = 0; j < MATRIX_HEIGHT; j++) {

      uint8_t data =  noise[layer][i][j] ;

      if ( data >= lower_limit  && data <= upper_limit) {

        CRGB pixel = ColorFromPalette( currentPalette, colorrepeat * (data + colorshift), data );

        leds[XY(index, i,j)] = pixel;
      }
    }
  }
}


uint16_t XY(uint8_t index, uint8_t x, uint8_t y) {
  if (x >= MATRIX_WIDTH) { x = MATRIX_WIDTH-1; }
  if (y >= MATRIX_HEIGHT) { y = MATRIX_HEIGHT-1; }
  if (index == 0) {
    // top left shoulderpad
    uint16_t i = y*MATRIX_WIDTH;
    if (y % 2 == 1) {
      i = i + MATRIX_WIDTH - x - 1;
    } else {
      i = i + x;
    }
    return i;
  } else if (index == 1) {
    // top right shoulderpad
    uint16_t i = (NUM_LEDS_PER_STRIP * index) + (x*MATRIX_WIDTH);
    if (x % 2 == 1) {
      i = i + MATRIX_WIDTH - y - 1;
    } else {
      i = i + y;
    }
    return i;
  } else if (index == 2) {
    // front left
    uint16_t i = (NUM_LEDS_PER_STRIP * index) + (y*MATRIX_WIDTH);
    if (y % 2 == 1) {
      i = i + MATRIX_WIDTH - x - 1;
    } else {
      i = i + x;
    }
    return i;
  } else if (index == 3) {
    // front right
    uint16_t i = (NUM_LEDS_PER_STRIP * index) + (y*MATRIX_WIDTH);
    if (y % 2 == 1) {
      i = i + x;
    } else {
      i = i + MATRIX_WIDTH - x - 1;
    }
    return i;
  }
  return NUM_LEDS-1;
}

uint16_t POS(uint8_t index, uint8_t i) {
  if (index == 0) {
    return i;
  } else if (index == 1) {
    uint8_t y = i / MATRIX_WIDTH;
    uint8_t x = i - (y * MATRIX_WIDTH);
    if (y % 2 == 1) {
      return XY(index, MATRIX_WIDTH - x - 1, y);
    } else {
      return XY(index, x, y);
    }
  } else if (index == 2) {
    return NUM_LEDS_PER_STRIP*2 + i;
  } else if (index == 3) {
    uint8_t y = i / MATRIX_WIDTH;
    uint8_t x = i - (y * MATRIX_WIDTH);
    if (y % 2 == 1) {
      return XY(index, MATRIX_WIDTH - x - 1, y);
    } else {
      return XY(index, x, y);
    }
  }
  return NUM_LEDS-1;
}




// Bresenham line algorythm based on 2 coordinates
void Line(uint8_t panel, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, byte color) {
    int dx = abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
    int dy = -abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
    int err = dx + dy, e2;
    for (;;) {
        leds[XY(panel, x0, y0)] = CHSV(color, 255, 255);
        if (x0 == x1 && y0 == y1) break;
        e2 = 2 * err;
        if (e2 > dy) { err += dy; x0 += sx; }
        if (e2 < dx) { err += dx; y0 += sy; }
    }
}
void Pixel(int x, int y, byte color) {
    for (uint8_t p = 0; p < NUM_STRIPS; p++) {
      leds[XY(p, x, y)] = CHSV(color, 255, 255);
    }
}
void DimAll(byte value)
{
    for (int i = 0; i < NUM_LEDS; i++)
    {
        leds[i].nscale8(value);
    }
}
void SpiralStream(int x, int y, int r, byte dimm) {
    for (uint8_t p = 0; p < NUM_STRIPS; p++) {
      for (int d = r; d >= 0; d--) { // from the outside to the inside
          for (int i = x - d; i <= x + d; i++) {
              leds[XY(p, i, y - d)] += leds[XY(p, i + 1, y - d)]; // lowest row to the right
              leds[XY(p, i, y - d)].nscale8(dimm);
          }
          for (int i = y - d; i <= y + d; i++) {
              leds[XY(p, x + d, i)] += leds[XY(p, x + d, i + 1)]; // right colum up
              leds[XY(p, x + d, i)].nscale8(dimm);
          }
          for (int i = x + d; i >= x - d; i--) {
              leds[XY(p, i, y + d)] += leds[XY(p, i - 1, y + d)]; // upper row to the left
              leds[XY(p, i, y + d)].nscale8(dimm);
          }
          for (int i = y + d; i >= y - d; i--) {
              leds[XY(p, x - d, i)] += leds[XY(p, x - d, i - 1)]; // left colum down
              leds[XY(p, x - d, i)].nscale8(dimm);
          }
      }
    }
}
// give it a linear tail to the side
void HorizontalStream(byte scale)
{
    for (uint8_t p = 0; p < NUM_STRIPS; p++) {
      for (uint8_t x = 1; x < MATRIX_WIDTH; x++) {
          for (uint8_t y = 0; y < MATRIX_HEIGHT; y++) {
              leds[XY(p, x, y)] += leds[XY(p, x - 1, y)];
              leds[XY(p, x, y)].nscale8(scale);
          }
      }
      for (uint8_t y = 0; y < MATRIX_HEIGHT; y++) {
        leds[XY(p, 0, y)].nscale8(scale);
      }
    }
}

// give it a linear tail downwards
void VerticalStream(byte scale)
{
    for (uint8_t p = 0; p < NUM_STRIPS; p++) {
      for (int x = 0; x < MATRIX_WIDTH; x++) {
          for (int y = 1; y < MATRIX_HEIGHT; y++) {
                leds[XY(p, x, y)] += leds[XY(p, x, y - 1)];
                leds[XY(p, x, y)].nscale8(scale);
          }
      }
      for (int x = 0; x < MATRIX_WIDTH; x++) {
          leds[XY(p, x, 0)].nscale8(scale);
      }
    }
}

// just move everything one line down
void VerticalMove() {
    for (int y = MATRIX_HEIGHT - 1; y > 0; y--) {
        for (int x = 0; x < MATRIX_WIDTH; x++) {
            for (uint8_t p = 0; p < NUM_STRIPS; p++) {
              leds[XY(p, x, y)] = leds[XY(p, x, y - 1)];
            }
        }
    }
}

// copy the rectangle defined with 2 points x0, y0, x1, y1
// to the rectangle beginning at x2, x3
void Copy(byte x0, byte y0, byte x1, byte y1, byte x2, byte y2) {
    for (int y = y0; y < y1 + 1; y++) {
        for (int x = x0; x < x1 + 1; x++) {
            for (uint8_t p = 0; p < NUM_STRIPS; p++) {
              leds[XY(p, x + x2 - x0, y + y2 - y0)] = leds[XY(p, x, y)];
            }
        }
    }
}

// rotate + copy triangle (MATRIX_WIDTH / 2*MATRIX_WIDTH / 2)
void RotateTriangle() {
    int halfWidth = MATRIX_WIDTH / 2;
    int halfWidthMinus1 = halfWidth - 1;

    for (int x = 1; x < halfWidth; x++) {
        for (int y = 0; y < x; y++) {
            for (uint8_t p = 0; p < NUM_STRIPS; p++) {
              leds[XY(p, x, halfWidthMinus1 - y)] = leds[XY(p, halfWidthMinus1 - x, y)];
            }
        }
    }
}

// mirror + copy triangle (MATRIX_WIDTH / 2*MATRIX_WIDTH / 2)
void MirrorTriangle() {
    int halfWidth = MATRIX_WIDTH / 2;
    int halfWidthMinus1 = halfWidth - 1;

    for (int x = 1; x < halfWidth; x++) {
        for (int y = 0; y < x; y++) {
            for (uint8_t p = 0; p < NUM_STRIPS; p++) {
              leds[XY(p, halfWidthMinus1 - y, x)] = leds[XY(p, halfWidthMinus1 - x, y)];
            }
        }
    }
}
// draw static rainbow triangle pattern (MATRIX_WIDTH / 2xMATRIX_WIDTH / 2)
// (just for debugging)
void RainbowTriangle() {
    int halfWidth = MATRIX_WIDTH / 2;
    int halfWidthMinus1 = halfWidth - 1;

    for (int i = 0; i < halfWidth; i++) {
        for (int j = 0; j <= i; j++) {
            Pixel(halfWidthMinus1 - i, j, i*j * 4);
        }
    }
}

void MoveOscillators() {
    osci[0] = osci[0] + 5;
    osci[1] = osci[1] + 2;
    osci[2] = osci[2] + 3;
    osci[3] = osci[3] + 4;
    for (int i = 0; i < 4; i++) {
        pa[i] = map(sin8(osci[i]), 0, 255, 0, MATRIX_WIDTH - 1); //why? to keep the result in the range of 0-MATRIX_WIDTH (matrix size)
    }
}